import React, { Component } from 'react'
import { View, Text, Alert, Button, StyleSheet } from 'react-native'
import { Link } from 'react-router-native'


class Screen1 extends Component {
    state = { usernames: '' }
    gotOscreen2 = () => {
        this.props.history.push('/screen2', this.props.location.state)
    }
    gotoLogin = () => {
        this.props.history.push('/loginpage', this.props.location.state)
    }
    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.usernames) {
            this.setState({ usernames: this.props.location.state.usernames })
        }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>


                <View style={{ flex: 1, backgroundColor: 'black' }} >
                    <Text  style={styles.textColor}>Profile</Text>
                    <Text  style={styles.textColor}>username</Text>
                    <Text style={styles.textColor}>
                        {this.state.usernames}
                    </Text>
                    <View><Button color='blue' title="GO GO => 2" onPress={this.gotOscreen2} /></View>
                    <View><Button title="Logout" onPress={this.gotoLogin} /></View>
                    
                </View>





            </View>
        )
    }
}

export default Screen1


const styles = StyleSheet.create({
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'grey',
        color: 'white'
    },
    textColor:{
        color: 'white' 
    }


})