import React, { Component }  from 'react'
import {View,Text, Button} from 'react-native'

class Screen2 extends Component {
    gotOscreen1 = () =>{
        this.props.history.push('/screen1', this.props.location.state)
    }
    UNSAFE_componentWillMount(){
        console.log(this.props)
        if(this.props.location && this.props.location.state&& this.props.location.state.usernames){
                this.setState({usernames:this.props.location.state.usernames})
        }
    }
    render(){
        return(
            <View style= {{flex: 1,backgroundColor:'grey'}}>
                <Text style = {{color: 'white'}}>
                    Screen 2
                </Text>

                <Text style={{ color: 'white' }}>
                  {this.state.usernames}
                </Text>

                <Button title= "GO GO => 1" onPress={this.gotOscreen1}/>
            </View>
        )
    }
}

export default Screen2