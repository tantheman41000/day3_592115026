import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Screen1 from './screen1'
import Screen2 from './screen2'
import Login from './loginPage'
class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/screen1" component={Screen1}/>
                    <Route exact path="/screen2" component={Screen2}/>
                    <Route exact path="/loginpage" component={Login}/>
                    <Redirect to ="/loginpage"/>
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router 